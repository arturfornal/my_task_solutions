#!/usr/bin/python
# -*- coding: iso-8859-15 -*-
'''
PermMissingElem

URL: https://codility.com/demo/results/demoDY4MY9-C3X/
ID: demoDY4MY9-C3X
Time limit: 120 min.
Created on: 2014-07-12 17:53 UTC
Started on: 2014-07-12 17:53 UTC
Finished on: 2014-07-12 18:06 UTC
Notes:
Done in home.

Task:
A zero-indexed array A consisting of N different integers is given. The array contains integers in the range [1..(N + 1)], which means that exactly one element is missing.
Your goal is to find that missing element.
Write a function:
def solution(A)
that, given a zero-indexed array A, returns the value of the missing element.
For example, given array A such that:
  A[0] = 2
  A[1] = 3
  A[2] = 1
  A[3] = 5
the function should return 4, as it is the missing element.
Assume that:
N is an integer within the range [0..100,000];
the elements of A are all distinct;
each element of array A is an integer within the range [1..(N + 1)].
Complexity:
expected worst-case time complexity is O(N);
expected worst-case space complexity is O(1), beyond input storage (not counting the storage required for input arguments).
Elements of input arrays can be modified.
Copyright 2009�2014 by Codility Limited. All Rights Reserved. Unauthorized copying, publication or disclosure prohibited.
'''

def solution(A):
	N = len(A)
	if N>100000 or N<0:
		return
	A.sort()
	if N == 0:
		return 1
	index = 0
	for i in range(N):
		if A[i]<1 or A[i]>N+1:
			return
		if A[i] == index:
			return
		if A[i] == i+1:
			index = i+1
		else:
			index = i+1
			break
	if not A[0] == 1:
		return 1
	if not A[N-1] == N+1:
		return N+1
	return index