#!/usr/bin/python
# -*- coding: iso-8859-15 -*-
'''
TapeEquilibrium

URL: https://codility.com/demo/results/demoT598ZB-AZV/
ID: demoT598ZB-AZV
Time limit: 120 min.
Created on: 2014-07-10 10:53 UTC
Started on: 2014-07-10 10:53 UTC
Finished on: 2014-07-10 12:50 UTC
Notes:
I made it at work in free time, few times I had to focus on different things but clock was ticking.

Task:
A non-empty zero-indexed array A consisting of N integers is given. Array A represents numbers on a tape.
Any integer P, such that 0 < P < N, splits this tape into two non-empty parts: A[0], A[1], ..., A[P − 1] and A[P], A[P + 1], ..., A[N − 1].
The difference between the two parts is the value of: |(A[0] + A[1] + ... + A[P − 1]) − (A[P] + A[P + 1] + ... + A[N − 1])|
In other words, it is the absolute difference between the sum of the first part and the sum of the second part.
For example, consider array A such that:
  A[0] = 3
  A[1] = 1
  A[2] = 2
  A[3] = 4
  A[4] = 3
We can split this tape in four places:
P = 1, difference = |3 − 10| = 7 
P = 2, difference = |4 − 9| = 5 
P = 3, difference = |6 − 7| = 1 
P = 4, difference = |10 − 3| = 7 
Write a function:
def solution(A)
that, given a non-empty zero-indexed array A of N integers, returns the minimal difference that can be achieved.
For example, given:
  A[0] = 3
  A[1] = 1
  A[2] = 2
  A[3] = 4
  A[4] = 3
the function should return 1, as explained above.
Assume that:
N is an integer within the range [2..100,000];
each element of array A is an integer within the range [−1,000..1,000].
Complexity:
expected worst-case time complexity is O(N);
expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
Elements of input arrays can be modified.
Copyright 2009–2014 by Codility Limited. All Rights Reserved. Unauthorized copying, publication or disclosure prohibited.
'''

# you can use print for debugging purposes, e.g.
# print "this is a debug message"

def solution(A):
    B = A[:]
    # write your code in Python 2.7
    N = len(A)
    if N < 2 or N > 100000:
        return
    for element_id in range(N):
        if element_id < N-1:
            P = element_id + 1
        if not isinstance( A[element_id], int ):
            return
        if A[element_id] < -1000 or A[element_id] > 1000:
            return
        
        if element_id > 0 and element_id < N-1:
            cid = N-1-element_id
            id = (N-1)-element_id+1
            B[cid] = B[cid] + B[id]
            A[element_id] = A[element_id-1]+A[element_id]
    del B[0]
    del A[N-1]
    for i in range(len(A)):
        A[i] = abs(A[i] - B[i])
    return min(A)