#!/usr/bin/python
# -*- coding: iso-8859-15 -*-
'''
FrogJmp

URL: https://codility.com/demo/results/demoDRK8GD-YQZ/
ID: demoDRK8GD-YQZ
Time limit: 120 min.
Created on: 2014-07-11 11:30 UTC
Started on: 2014-07-11 11:30 UTC
Finished on: 2014-07-11 11:59 UTC
Notes:
I made it at work in free time, few times I had to focus on different things but clock was ticking.

Task:
A small frog wants to get to the other side of the road. The frog is currently located at position X and wants to get to a position greater than or equal to Y. The small frog always jumps a fixed distance, D.
Count the minimal number of jumps that the small frog must perform to reach its target.
Write a function:
def solution(X, Y, D)
that, given three integers X, Y and D, returns the minimal number of jumps from position X to a position equal to or greater than Y.
For example, given:
  X = 10
  Y = 85
  D = 30
the function should return 3, because the frog will be positioned as follows:
after the first jump, at position 10 + 30 = 40
after the second jump, at position 10 + 30 + 30 = 70
after the third jump, at position 10 + 30 + 30 + 30 = 100
Assume that:
X, Y and D are integers within the range [1..1,000,000,000];
X ≤ Y.
Complexity:
expected worst-case time complexity is O(1);
expected worst-case space complexity is O(1).
Copyright 2009–2014 by Codility Limited. All Rights Reserved. Unauthorized copying, publication or disclosure prohibited.
'''

# you can use print for debugging purposes, e.g.
# print "this is a debug message"

def solution(X, Y, D):
    # write your code in Python 2.7
    if not isinstance(X, int):
        return
    if not isinstance(Y, int):
        return
    if not isinstance(D, int):
        return
    if X<1 or X>1000000000 or Y<1 or Y>1000000000 or D<1 or D>1000000000 or X>Y:
        return
    result = (Y-X)//D#
    if ((Y-X)%D) > 0:
        result +=1
    return result